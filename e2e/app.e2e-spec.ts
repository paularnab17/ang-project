import { AjProPage } from './app.po';

describe('aj-pro App', () => {
  let page: AjProPage;

  beforeEach(() => {
    page = new AjProPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
